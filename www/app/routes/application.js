import Ember from 'ember';
import config from '../config/environment';

function selectLocale(selected) {
  // FIXME
  let supported = ['en', 'ar-sa', 'en-us'];
  const language = navigator.languages[0] || navigator.language || navigator.userLanguage;

  let locale = selected;

  if (locale == null) {
    // default locale
    locale = language;
    if (supported.indexOf(locale) < 0) {
      locale = locale.replace(/\-[a-zA-Z]*$/, '');
    }
  }
  if (supported.indexOf(locale) >= 0) {
    if (locale === 'en') {
      locale = 'en-us';
    }
  } else {
    locale = 'en-us';
  }
  return locale;
}


export default Ember.Route.extend({
  intl: Ember.inject.service(),
  selectedLanguage: null,
<<<<<<< HEAD
  languages: null,
=======
  poolSettings: null,

>>>>>>> 0abfd95... www/app: read the external pool settings from /api/settings
  beforeModel() {
     let locale = this.get('selectedLanguage');
     if (!locale) {
       // read cookie
       locale = Ember.$.cookie('lang');
       // pick a locale
       locale = selectLocale(locale);

<<<<<<< HEAD
       this.get('intl').setLocale(locale);
       Ember.$.cookie('lang', locale);
       console.log('INFO: locale selected - ' + locale);
       this.set('selectedLanguage', locale);
     }
     let intl = this.get('intl');
     this.set('languages', [
      { name: intl.t('lang.arabic'), value: 'ar-sa'},
      { name: intl.t('lang.english'), value: 'en-us'}
    ]);
   },
=======
      this.get('intl').setLocale(locale);
      Ember.$.cookie('lang', locale);
      console.log('INFO: locale selected - ' + locale);
      this.set('selectedLanguage', locale);
    }

    let settings = this.get('poolSettings');
    if (!settings) {
      let self = this;
      let url = config.APP.ApiUrl + 'api/settings';
      Ember.$.ajax({
        url: url,
        type: 'GET',
        header: {
          'Accept': 'application/json'
        },
        success: function(data) {
          settings = Ember.Object.create(data);
          self.set('poolSettings', settings);
          console.log('INFO: pool settings loaded..');
        },
        error: function(request, status, e) {
          console.log('ERROR: fail to load pool settings: ' + e);
          self.set('poolSettings', {});
        }
      });
    }
  },
>>>>>>> 0abfd95... www/app: read the external pool settings from /api/settings

   actions: {
     selectLanguage: function(lang) {
       let selected = lang;
       if (typeof selected === 'undefined') {
         return true;
       }
       let locale = selectLocale(selected);
       this.get('intl').setLocale(locale);
       this.set('selectedLanguage', locale);
       Ember.$.cookie('lang', locale);
       let languages = this.get('languages');
       for (var i = 0; i < languages.length; i++) {
        if (languages[i].value === locale) {
          Ember.$('#selectedLanguage').html(languages[i].name + '<b class="caret"></b>');
          break;
        }
      }


       return true;
   },

    toggleMenu: function() {
      Ember.$('.navbar-collapse.in').attr("aria-expanded", false).removeClass("in");
    }
  },

	model: function() {
    var url = config.APP.ApiUrl + 'api/stats';
    return Ember.$.getJSON(url).then(function(data) {
      return Ember.Object.create(data);
    });
	},

  setupController: function(controller, model) {
    let settings = this.get('poolSettings');
    model.settings = settings;
    this._super(controller, model);
    Ember.run.later(this, this.refresh, 5000);
    model.languages = this.get('languages');
  }
});
