#!/bin/bash

#pool2b:
echo starting 2b pool
screen -dmS 2b ./egem 2b.json

#pool4b:
echo starting 4b pool
screen -dmS 4b ./egem 4b.json

#pool9b:
echo starting 9b pool
screen -dmS 9b ./egem 9b.json

#api:
echo starting API
screen -dmS api ./egem api.json

#unlocker:
echo starting unlocker
screen -dmS unlocker ./egem unlocker.json

#payout:
echo starting payout
screen -dmS payouts ./egem payouts.json
